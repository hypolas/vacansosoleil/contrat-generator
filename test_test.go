package contratgen

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

func TestCreateDoc(t *testing.T) {
	cont := Contact{}
	testDate, _ := os.ReadFile("test.json")
	json.Unmarshal(testDate, &cont)

	fi, _ := os.ReadFile("be.docx")

	do, fname := cont.GenerateTemplate(fi, "25-36-2003")

	os.WriteFile(fname, do, os.ModePerm)
}

func TestDateParse(t *testing.T) {
	ti, err := time.Parse("2/1/2006", "03/11/2023")
	if err != nil {
		log.Fatal(err)
	}
	payBefore := ti.Add(-24 * 30 * time.Hour)
	fmt.Println(payBefore.Format("02/01/2006"))
}
