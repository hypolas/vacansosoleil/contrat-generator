package contratgen

import (
	"bytes"
	"encoding/json"
	docx "github.com/lukasjarosch/go-docx"
	"gitlab.com/hypolas/vacansosoleil/go-types"
	"log"
	"strconv"
	"strings"
	"time"
)

type Contact types.Contact

type NosGites struct {
	ID          string `json:"id"`
	Initial     string `json:"initial"`
	Maxclient   int    `json:"maxclient"`
	Nom         string `json:"nom"`
	Tarif       int    `json:"tarif"`
	TarifSejour int    `json:"tarifSejour"`
	URLFolder   string `json:"url_folder"`
}

func (c *Contact) GenerateTemplate(contrat []byte, dateNow string) (fichierDoc []byte, nomFichier string) {
	var DataJson string = `{
		"to": {
			"id": "1",
			"initial": "to",
			"maxclient": 4,
			"url_folder": "la-topaze-d-or",
			"nom": "La Topaze d'Or",
			"tarif": 50,
			"tarifSejour": 50
		},
		"js": {
			"id": "2",
			"initial": "js",
			"maxclient": 4,
			"url_folder": "le-joyeux-saphir",
			"nom": "Le Joyeux Saphir",
			"tarif": 50
		},
		"ra": {
			"id": "3",
			"initial": "ra",
			"nom": "Le Rubis Ardent",
			"maxclient": 5,
			"url_folder": "le-rubis-ardent",
			"tarif": 60,
			"tarifSejour": 60
		},
		"be": {
			"id": "4",
			"initial": "be",
			"nom": "La Belle Emeraude",
			"maxclient": 6,
			"url_folder": "la-belle-emeraude",
			"tarif": 80,
			"tarifSejour": 80
		},
		"jp": {
			"id": "5",
			"initial": "jp",
			"nom": "Le Jupiter",
			"maxclient": 8,
			"url_folder": "le-jupiter",
			"tarif": 90,
			"tarifSejour": 90
		}
	}`

	var DataGites map[string]NosGites = make(map[string]NosGites)

	json.Unmarshal([]byte(DataJson), &DataGites)

	var err error
	// Prix au dela de 4 personnes
	var addPersonne int
	personneSup := c.NombrePersonnes - 4
	if personneSup > 0 {
		addPersonne = c.NombrePersonnes - 4
	}
	c.MontantSejour = (DataGites[c.GiteInitial].Tarif * c.NombreNuits) + (c.NombreNuits * addPersonne * 10)

	// Calcul de la date de paiement
	ti, _ := time.Parse("2/1/2006", c.SejourDebut)

	if c.PayBefore == "" {
		payBefore := ti.Add(-24 * 30 * time.Hour)
		c.PayBefore = payBefore.Format("02/01/2006")
	}

	// Définite les variable pour le template
	replaceMap := docx.PlaceholderMap{
		"payBefore":       c.PayBefore,
		"startDate":       c.SejourDebut,
		"endDate":         c.SejourFin,
		"nbNuit":          c.NombreNuits,
		"tarifSejour":     c.MontantSejour,
		"arrhes":          int(float64(c.MontantSejour) * 0.3),
		"solde":           strconv.Itoa(c.MontantSejour - int(float64(c.MontantSejour)*0.3)),
		"clientNom":       c.ClientNom,
		"clientPrenom":    c.ClientPrenom,
		"clientAdresse":   c.ClientAdresse,
		"nombrePersonnes": c.NombrePersonnes,
	}

	var doc *docx.Document

	doc, err = docx.OpenBytes(contrat)
	if err != nil {
		log.Fatal(err)
	}

	err = doc.ReplaceAll(replaceMap)
	if err != nil {
		log.Println(err)
	}

	dateFile := strings.Split(c.SejourDebut, "/")
	reversedSlice := make([]string, len(dateFile))
	copy(reversedSlice, dateFile[:])

	// Reverse the new slice in place
	for i, j := 0, len(reversedSlice)-1; i < j; i, j = i+1, j-1 {
		reversedSlice[i], reversedSlice[j] = reversedSlice[j], reversedSlice[i]
	}

	nomFIchier := strings.Join(reversedSlice, "_") + "_" + c.GiteInitial + "_" + c.ClientNom + "_" + c.ClientPrenom + ".docx"

	var docfile bytes.Buffer

	doc.Write(&docfile)

	return docfile.Bytes(), nomFIchier
}
