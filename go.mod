module gitlab.com/hypolas/vacansosoleil/contrat-generator

go 1.22.2

require (
	github.com/lukasjarosch/go-docx v0.4.7
	gitlab.com/hypolas/vacansosoleil/go-types v0.0.10
)

require golang.org/x/net v0.27.0 // indirect
